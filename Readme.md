# Saber PublicUrl Bug Repoduction

This is the code required to reproduce the `publicUrl` issue with saber.

Just check out the gitlab pages to see the bug in action:

https://morgul.gitlab.io/saber-publicurl-bug
